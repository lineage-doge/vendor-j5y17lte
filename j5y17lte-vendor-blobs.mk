# Copyright (C) 2017 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# This file is generated by device/samsung/j5y17lte/setup-makefiles.sh

PRODUCT_COPY_FILES += \
    vendor/samsung/j5y17lte/proprietary/etc/bluetooth/av_performance.conf:system/etc/bluetooth/av_performance.conf \
    vendor/samsung/j5y17lte/proprietary/etc/bluetooth/bt_did.conf:system/etc/bluetooth/bt_did.conf \
    vendor/samsung/j5y17lte/proprietary/etc/bluetooth/bt_stack.conf:system/etc/bluetooth/bt_stack.conf \
    vendor/samsung/j5y17lte/proprietary/etc/bluetooth/iop_bt.db:system/etc/bluetooth/iop_bt.db \
    vendor/samsung/j5y17lte/proprietary/etc/bluetooth/iop_device_list.conf:system/etc/bluetooth/iop_device_list.conf \
    vendor/samsung/j5y17lte/proprietary/vendor/etc/ca.pem:system/vendor/etc/ca.pem \
    vendor/samsung/j5y17lte/proprietary/vendor/etc/plmn_delta.bin:system/vendor/etc/plmn_delta.bin \
    vendor/samsung/j5y17lte/proprietary/vendor/etc/plmn_se13.bin:system/vendor/etc/plmn_se13.bin \
    vendor/samsung/j5y17lte/proprietary/etc/srm.bin:system/etc/srm.bin \
    vendor/samsung/j5y17lte/proprietary/lib/hw/audio.primary.universal7870.so:system/lib/hw/audio.primary.universal7870.so \
    vendor/samsung/j5y17lte/proprietary/lib/hw/camera.universal7870.so:system/lib/hw/camera.universal7870.so \
    vendor/samsung/j5y17lte/proprietary/vendor/lib/hw/gps.default.so:system/vendor/lib/hw/gps.default.so \
    vendor/samsung/j5y17lte/proprietary/lib/hw/gralloc.exynos5.so:system/lib/hw/gralloc.exynos5.so \
    vendor/samsung/j5y17lte/proprietary/vendor/lib/hw/sensors.universal7870.so:system/vendor/lib/hw/sensors.universal7870.so \
    vendor/samsung/j5y17lte/proprietary/lib/lib_DNSe_EP_ver216c.so:system/lib/lib_DNSe_EP_ver216c.so \
    vendor/samsung/j5y17lte/proprietary/lib/lib_SamsungRec_06003.so:system/lib/lib_SamsungRec_06003.so \
    vendor/samsung/j5y17lte/proprietary/lib/lib_SoundAlive_SRC384_ver300.so:system/lib/lib_SoundAlive_SRC384_ver300.so \
    vendor/samsung/j5y17lte/proprietary/lib/lib_soundaliveresampler.so:system/lib/lib_soundaliveresampler.so \
    vendor/samsung/j5y17lte/proprietary/lib/lib_SoundBooster_ver800.so:system/lib/lib_SoundBooster_ver800.so \
    vendor/samsung/j5y17lte/proprietary/lib/libaudio-ril.so:system/lib/libaudio-ril.so \
    vendor/samsung/j5y17lte/proprietary/lib/libaudioroute.so:system/lib/libaudioroute.so \
    vendor/samsung/j5y17lte/proprietary/vendor/lib/libbt-vendor.so:system/vendor/lib/libbt-vendor.so \
    vendor/samsung/j5y17lte/proprietary/lib/libcamera_metadata.so:system/lib/libcamera_metadata.so \
    vendor/samsung/j5y17lte/proprietary/lib/libcodecsolution.so:system/lib/libcodecsolution.so \
    vendor/samsung/j5y17lte/proprietary/lib/libcsc.so:system/lib/libcsc.so \
    vendor/samsung/j5y17lte/proprietary/lib/libexynoscamera.so:system/lib/libexynoscamera.so \
    vendor/samsung/j5y17lte/proprietary/lib/libexynoscamera3.so:system/lib/libexynoscamera3.so \
    vendor/samsung/j5y17lte/proprietary/lib/hw/hwcomposer.exynos5.so:system/lib/hw/hwcomposer.exynos5.so \
    vendor/samsung/j5y17lte/proprietary/lib/libexynosdisplay.so:system/lib/libexynosdisplay.so \
    vendor/samsung/j5y17lte/proprietary/lib/libexynosgscaler.so:system/lib/libexynosgscaler.so \
    vendor/samsung/j5y17lte/proprietary/lib/libexynosscaler.so:system/lib/libexynosscaler.so \
    vendor/samsung/j5y17lte/proprietary/lib/libexynosv4l2.so:system/lib/libexynosv4l2.so \
    vendor/samsung/j5y17lte/proprietary/lib/libfloatingfeature.so:system/lib/libfloatingfeature.so \
    vendor/samsung/j5y17lte/proprietary/lib/libhwjpeg.so:system/lib/libhwjpeg.so \
    vendor/samsung/j5y17lte/proprietary/lib/libpreprocessing_nxp.so:system/lib/libpreprocessing_nxp.so \
    vendor/samsung/j5y17lte/proprietary/lib/libprotobuf-cpp-fooo.so:system/lib/libprotobuf-cpp-fooo.so \
    vendor/samsung/j5y17lte/proprietary/vendor/lib/libreference-ril.so:system/vendor/lib/libreference-ril.so \
    vendor/samsung/j5y17lte/proprietary/vendor/lib/librilutils.so:system/vendor/lib/librilutils.so \
    vendor/samsung/j5y17lte/proprietary/lib/libSamsungPostProcess.so:system/lib/libSamsungPostProcess.so \
    vendor/samsung/j5y17lte/proprietary/lib/libSamsungPostProcessConvertor.so:system/lib/libSamsungPostProcessConvertor.so \
    vendor/samsung/j5y17lte/proprietary/lib/libsamsungSoundbooster_plus.so:system/lib/libsamsungSoundbooster_plus.so \
    vendor/samsung/j5y17lte/proprietary/lib/libsecaudioinfo.so:system/lib/libsecaudioinfo.so \
    vendor/samsung/j5y17lte/proprietary/lib/libseccameracore.so:system/lib/libseccameracore.so \
    vendor/samsung/j5y17lte/proprietary/lib/libsecnativefeature.so:system/lib/libsecnativefeature.so \
    vendor/samsung/j5y17lte/proprietary/vendor/lib/libsec-ril.so:system/vendor/lib/libsec-ril.so \
    vendor/samsung/j5y17lte/proprietary/vendor/lib/libsec-ril-dsds.so:system/vendor/lib/libsec-ril-dsds.so \
    vendor/samsung/j5y17lte/proprietary/lib/libsecril-client.so:system/lib/libsecril-client.so \
    vendor/samsung/j5y17lte/proprietary/lib/libsensorlistener.so:system/lib/libsensorlistener.so \
    vendor/samsung/j5y17lte/proprietary/lib/libsensorservice.so:system/lib/libsensorservice.so \
    vendor/samsung/j5y17lte/proprietary/vendor/lib/libstainkiller.so:system/vendor/lib/libstainkiller.so \
    vendor/samsung/j5y17lte/proprietary/lib/libtinyalsa.so:system/lib/libtinyalsa.so \
    vendor/samsung/j5y17lte/proprietary/vendor/lib/libtinycompress.so:system/vendor/lib/libtinycompress.so \
    vendor/samsung/j5y17lte/proprietary/lib/libuniplugin.so:system/lib/libuniplugin.so \
    vendor/samsung/j5y17lte/proprietary/lib/libvulkan.so:system/lib/libvulkan.so \
    vendor/samsung/j5y17lte/proprietary/vendor/lib/libwrappergps.so:system/vendor/lib/libwrappergps.so \
    vendor/samsung/j5y17lte/proprietary/lib/omx/libOMX.Exynos.AVC.Decoder.so:system/lib/omx/libOMX.Exynos.AVC.Decoder.so \
    vendor/samsung/j5y17lte/proprietary/lib/omx/libOMX.Exynos.AVC.Encoder.so:system/lib/omx/libOMX.Exynos.AVC.Encoder.so \
    vendor/samsung/j5y17lte/proprietary/lib/omx/libOMX.Exynos.HEVC.Decoder.so:system/lib/omx/libOMX.Exynos.HEVC.Decoder.so \
    vendor/samsung/j5y17lte/proprietary/lib/omx/libOMX.Exynos.HEVC.Encoder.so:system/lib/omx/libOMX.Exynos.HEVC.Encoder.so \
    vendor/samsung/j5y17lte/proprietary/lib/omx/libOMX.Exynos.MPEG4.Decoder.so:system/lib/omx/libOMX.Exynos.MPEG4.Decoder.so \
    vendor/samsung/j5y17lte/proprietary/lib/omx/libOMX.Exynos.MPEG4.Encoder.so:system/lib/omx/libOMX.Exynos.MPEG4.Encoder.so \
    vendor/samsung/j5y17lte/proprietary/lib/omx/libOMX.Exynos.VP8.Decoder.so:system/lib/omx/libOMX.Exynos.VP8.Decoder.so \
    vendor/samsung/j5y17lte/proprietary/lib/omx/libOMX.Exynos.VP8.Encoder.so:system/lib/omx/libOMX.Exynos.VP8.Encoder.so \
    vendor/samsung/j5y17lte/proprietary/lib/omx/libOMX.Exynos.WMV.Decoder.so:system/lib/omx/libOMX.Exynos.WMV.Decoder.so \
    vendor/samsung/j5y17lte/proprietary/vendor/app/mcRegistry/FFFFFFFF000000000000000000000001.drbin:system/vendor/app/mcRegistry/FFFFFFFF000000000000000000000001.drbin \
    vendor/samsung/j5y17lte/proprietary/vendor/app/mcRegistry/07010000000000000000000000000000.tlbin:system/vendor/app/mcRegistry/07010000000000000000000000000000.tlbin \
    vendor/samsung/j5y17lte/proprietary/vendor/app/mcRegistry/ffffffff00000000000000000000000b.tlbin:system/vendor/app/mcRegistry/ffffffff00000000000000000000000b.tlbin \
    vendor/samsung/j5y17lte/proprietary/vendor/app/mcRegistry/ffffffff00000000000000000000002f.tlbin:system/vendor/app/mcRegistry/ffffffff00000000000000000000002f.tlbin \
    vendor/samsung/j5y17lte/proprietary/vendor/app/mcRegistry/ffffffffd0000000000000000000000e.tlbin:system/vendor/app/mcRegistry/ffffffffd0000000000000000000000e.tlbin \
    vendor/samsung/j5y17lte/proprietary/vendor/app/mcRegistry/07060000000000000000000000000000.tlbin:system/vendor/app/mcRegistry/07060000000000000000000000000000.tlbin \
    vendor/samsung/j5y17lte/proprietary/vendor/app/mcRegistry/ffffffff00000000000000000000000f.tlbin:system/vendor/app/mcRegistry/ffffffff00000000000000000000000f.tlbin \
    vendor/samsung/j5y17lte/proprietary/vendor/app/mcRegistry/ffffffff000000000000000000000030.tlbin:system/vendor/app/mcRegistry/ffffffff000000000000000000000030.tlbin \
    vendor/samsung/j5y17lte/proprietary/vendor/app/mcRegistry/ffffffffd00000000000000000000014.tlbin:system/vendor/app/mcRegistry/ffffffffd00000000000000000000014.tlbin \
    vendor/samsung/j5y17lte/proprietary/vendor/app/mcRegistry/08130000000000000000000000000000.tlbin:system/vendor/app/mcRegistry/08130000000000000000000000000000.tlbin \
    vendor/samsung/j5y17lte/proprietary/vendor/app/mcRegistry/ffffffff000000000000000000000012.tlbin:system/vendor/app/mcRegistry/ffffffff000000000000000000000012.tlbin \
    vendor/samsung/j5y17lte/proprietary/vendor/app/mcRegistry/ffffffff000000000000000000000038.tlbin:system/vendor/app/mcRegistry/ffffffff000000000000000000000038.tlbin \
    vendor/samsung/j5y17lte/proprietary/vendor/app/mcRegistry/ffffffffd00000000000000000000016.tlbin:system/vendor/app/mcRegistry/ffffffffd00000000000000000000016.tlbin \
    vendor/samsung/j5y17lte/proprietary/vendor/app/mcRegistry/ffffffff000000000000000000000013.tlbin:system/vendor/app/mcRegistry/ffffffff000000000000000000000013.tlbin \
    vendor/samsung/j5y17lte/proprietary/vendor/app/mcRegistry/ffffffff000000000000000000000041.tlbin:system/vendor/app/mcRegistry/ffffffff000000000000000000000041.tlbin \
    vendor/samsung/j5y17lte/proprietary/vendor/app/mcRegistry/ffffffff000000000000000000000005.tlbin:system/vendor/app/mcRegistry/ffffffff000000000000000000000005.tlbin \
    vendor/samsung/j5y17lte/proprietary/vendor/app/mcRegistry/ffffffff000000000000000000000017.tlbin:system/vendor/app/mcRegistry/ffffffff000000000000000000000017.tlbin \
    vendor/samsung/j5y17lte/proprietary/vendor/app/mcRegistry/ffffffff000000000000000000000073.tlbin:system/vendor/app/mcRegistry/ffffffff000000000000000000000073.tlbin \
    vendor/samsung/j5y17lte/proprietary/vendor/app/mcRegistry/ffffffff00000000000000000000000a.tlbin:system/vendor/app/mcRegistry/ffffffff00000000000000000000000a.tlbin \
    vendor/samsung/j5y17lte/proprietary/vendor/app/mcRegistry/ffffffff00000000000000000000002e.tlbin:system/vendor/app/mcRegistry/ffffffff00000000000000000000002e.tlbin \
    vendor/samsung/j5y17lte/proprietary/vendor/app/mcRegistry/ffffffffd0000000000000000000000a.tlbin:system/vendor/app/mcRegistry/ffffffffd0000000000000000000000a.tlbin \
    vendor/samsung/j5y17lte/proprietary/app/mcRegistry/00060308060501020000000000000000.tlbin:system/app/mcRegistry/00060308060501020000000000000000.tlbin \
    vendor/samsung/j5y17lte/proprietary/app/mcRegistry/ffffffff000000000000000000000004.tlbin:system/app/mcRegistry/ffffffff000000000000000000000004.tlbin \
    vendor/samsung/j5y17lte/proprietary/app/mcRegistry/ffffffff00000000000000000000000c.tlbin:system/app/mcRegistry/ffffffff00000000000000000000000c.tlbin \
    vendor/samsung/j5y17lte/proprietary/app/mcRegistry/ffffffff00000000000000000000000d.tlbin:system/app/mcRegistry/ffffffff00000000000000000000000d.tlbin \
    vendor/samsung/j5y17lte/proprietary/app/mcRegistry/ffffffff000000000000000000000016.tlbin:system/app/mcRegistry/ffffffff000000000000000000000016.tlbin \
    vendor/samsung/j5y17lte/proprietary/app/mcRegistry/ffffffff000000000000000000000019.tlbin:system/app/mcRegistry/ffffffff000000000000000000000019.tlbin \
    vendor/samsung/j5y17lte/proprietary/app/mcRegistry/ffffffff000000000000000000000045.tlbin:system/app/mcRegistry/ffffffff000000000000000000000045.tlbin \
    vendor/samsung/j5y17lte/proprietary/app/mcRegistry/ffffffffd00000000000000000000004.tlbin:system/app/mcRegistry/ffffffffd00000000000000000000004.tlbin \
    vendor/samsung/j5y17lte/proprietary/system/bin/gpsd:system/system/bin/gpsd \
    vendor/samsung/j5y17lte/proprietary/vendor/bin/mcDriverDaemon:system/vendor/bin/mcDriverDaemon \
    vendor/samsung/j5y17lte/proprietary/vendor/firmware/fimc_is_lib.bin:system/vendor/firmware/fimc_is_lib.bin \
    vendor/samsung/j5y17lte/proprietary/vendor/firmware/mfc_fw.bin:system/vendor/firmware/mfc_fw.bin \
    vendor/samsung/j5y17lte/proprietary/vendor/firmware/setfile_3m3.bin:system/vendor/firmware/setfile_3m3.bin \
    vendor/samsung/j5y17lte/proprietary/vendor/firmware/setfile_imx258.bin:system/vendor/firmware/setfile_imx258.bin \
    vendor/samsung/j5y17lte/proprietary/vendor/lib/egl/libGLES_mali.so:system/vendor/lib/egl/libGLES_mali.so \
    vendor/samsung/j5y17lte/proprietary/vendor/lib/libMcClient.so:system/vendor/lib/libMcClient.so \
    vendor/samsung/j5y17lte/proprietary/vendor/lib/libMcRegistry.so:system/vendor/lib/libMcRegistry.so \
    vendor/samsung/j5y17lte/proprietary/vendor/lib/libLifevibes_lvverx.so:system/vendor/lib/libLifevibes_lvverx.so \
    vendor/samsung/j5y17lte/proprietary/vendor/lib/libLifevibes_lvvetx.so:system/vendor/lib/libLifevibes_lvvetx.so \
    vendor/samsung/j5y17lte/proprietary/vendor/lib/mediadrm/libdrmclearkeyplugin.so:system/vendor/lib/mediadrm/libdrmclearkeyplugin.so \
    vendor/samsung/j5y17lte/proprietary/vendor/lib/mediadrm/libwvdrmengine.so:system/vendor/lib/mediadrm/libwvdrmengine.so \
    vendor/samsung/j5y17lte/proprietary/vendor/lib/libvndsecril-client.so:system/vendor/lib/libvndsecril-client.so \
    vendor/samsung/j5y17lte/proprietary/lib/libsamsungDiamondVoice.so:system/lib/libsamsungDiamondVoice.so \
    vendor/samsung/j5y17lte/proprietary/lib/libExynosHWCService.so:system/lib/libExynosHWCService.so \
    vendor/samsung/j5y17lte/proprietary/vendor/lib/libion_exynos.so:system/vendor/lib/libion_exynos.so \
    vendor/samsung/j5y17lte/proprietary/lib/libaptX_encoder.so:system/lib/libaptX_encoder.so \
    vendor/samsung/j5y17lte/proprietary/lib/hw/gralloc.exynos5.so:system/lib/hw/gralloc.exynos5.so \
    vendor/samsung/j5y17lte/proprietary/lib/libfimg.so:system/lib/libfimg.so \
    vendor/samsung/j5y17lte/proprietary/lib/libhdmi.so:system/lib/libhdmi.so \
    vendor/samsung/j5y17lte/proprietary/lib/libhwastc.so:system/lib/libhwastc.so \
    vendor/samsung/j5y17lte/proprietary/lib/libhwcutils.so:system/lib/libhwcutils.so \
    vendor/samsung/j5y17lte/proprietary/lib/libhwjpeg.so:system/lib/libhwjpeg.so \
    vendor/samsung/j5y17lte/proprietary/lib/librecordalive.so:system/lib/librecordalive.so \
    vendor/samsung/j5y17lte/proprietary/lib/libsamsungeffect.so:system/lib/libsamsungeffect.so \
    vendor/samsung/j5y17lte/proprietary/lib/libskia.so:system/lib/libskia.so \
    vendor/samsung/j5y17lte/proprietary/lib/libtfa98xx.so:system/lib/libtfa98xx.so \
    vendor/samsung/j5y17lte/proprietary/lib/libtinyxml2.so:system/lib/libtinyxml2.so \
    vendor/samsung/j5y17lte/proprietary/lib/libvirtualdisplay.so:system/lib/libvirtualdisplay.so \
    vendor/samsung/j5y17lte/proprietary/vendor/lib/libAstcEnc.so:system/vendor/lib/libAstcEnc.so \
    vendor/samsung/j5y17lte/proprietary/vendor/etc/Tfa9890.cnt:system/vendor/etc/Tfa9890.cnt \
    vendor/samsung/j5y17lte/proprietary/vendor/lib/libion_exynos.so:system/vendor/lib/libion_exynos.so \
    vendor/samsung/j5y17lte/proprietary/vendor/lib/libsensorndkbridge.so:system/vendor/lib/libsensorndkbridge.so \
    vendor/samsung/j5y17lte/proprietary/vendor/lib/libyasalgo.so:system/vendor/lib/libyasalgo.so
